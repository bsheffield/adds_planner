################################################################################
# Author:  Allison Price
# Company:  Naval Surface Warfare Center Panama City Division
# Date:  17 April 2019
################################################################################
cmake_minimum_required(VERSION 2.8.3)
project(adds_planner)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

################################################################################
# Find necessary ros packages needed for adds_planner
################################################################################
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  geometry_msgs
  tf2
  message_generation
)

################################################
## Declare ROS messages, services and actions ##
################################################

## Generate messages in the 'msg' folder
add_message_files(
   FILES
   OCUInputMsg.msg
   PlannerStatus.msg
)

## Generate added messages and services with any dependencies listed here
generate_messages(
   DEPENDENCIES
   std_msgs
   geometry_msgs
   # OCUInputMsg
)

###################################
## catkin specific configuration ##
###################################
catkin_package(
  INCLUDE_DIRS
    include
  LIBRARIES
  CATKIN_DEPENDS
    roscpp
    std_msgs
    geometry_msgs
    tf2
    # message_runtime
)

###########
## Build ##
###########

## Specify additional locations of header files
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ executable
set (BINNAME adds_planner)
FILE(GLOB SRC
  src/*.cpp
)

add_executable(
  ${BINNAME}
  ${SRC}
)

add_dependencies(
  ${BINNAME}
  adds_planner_generate_messages_cpp
)

## Specify libraries to link a library or executable target against
target_link_libraries(${BINNAME}
  ${catkin_LIBRARIES}
  ${SYSTEM_LIBS}
)
