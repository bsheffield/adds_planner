/***************************************************************************//**
 * @mainpage
 * @author Allison Price
 * @Date 05/17/2019
 * @version 1.0
 *
 * @brief Planner for Autonomous Dry Docks Survey (ADDS)
 *
 * When provided a four point boundary from the operator control unit, the
 * application generates a simple lawnmower waypoint path to be followed.
 *
 * This software was developed at Naval Surface Warfare Center Panama City
 ******************************************************************************/

#include "adds_planner/ADDSPlanner.h"

#include "tf2/LinearMath/Quaternion.h"

#include <string>
#include <sstream>
#include <iostream>
#include <cmath>

using namespace std;

/**
* @brief Constructor of ADDSPlanner class.
*
* The constructor of the ADDSPlanner class starts by creating the subscribers
* and publishers for the node.  It subscribes to /adds_planner/ocu_bounds to get
* the operating area from the operator and to /move_base/result to determine if a
* waypoint has been reached or missed.  The node will publish and initialpose,
* MoveBaseActionGoal's to /move_base/goal, and GoalID's to /move_base/cancel.
* Once the subs and pubs have been set up, the constructor gets the ros parameters
* from the launch file.  If there is an error reading in a parameter value, a
* message is printed in the terminal and a default value is used.
* If ros is okay, the method performs one spin.  If the OCU bounds have been been
* created and the survey is not executing, then check if the waypoint plan has been
* generated.  If no plan exists, then set the flag for executing a plan and call the
* create plan method.  If OCU bounds have been recieved and the survey is executing,
* the constructor checks if a plan has been created.  All these conditions being met
* means all the waypoints have been created in the PoseArray member variable and the
* node is ready to start publishing the waypoints as goals to move_base.  If the flag
* for publishing the next point is true and we have not published all the waypoint
* goals, then the method publishes the next waypoint goal to move base.  When the all
* the waypoints have been published as goals, a completion message is printed and the
* method is reset to accept new OCU bounds.  See comments in the code for details on
* more specific behaviors.
*/
ADDSPlanner::ADDSPlanner()
{
  // Set up subscribers
  m_ocuInputSub = m_nh.subscribe("/adds_planner/ocu_bounds", 100, &ADDSPlanner::OCUBoundsCallback, this);
  m_wayptResultSub = m_nh.subscribe("/move_base/result", 1000, &ADDSPlanner::WayPtResultCallback, this);

  // Set up publishers
  m_launchPtPub = m_nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 100, true);
  m_waypointPub = m_nh.advertise<move_base_msgs::MoveBaseActionGoal>("/move_base/goal", 1000, true);
  m_waypointCancelPub = m_nh.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1000, true);
  m_plannerStatusPub = m_nh.advertise<adds_planner::PlannerStatus>("/adds_planner/survey_status", 100, true);

  if (m_nh.getParam("/ADDSPlanner/IncrementValueX", m_incrementValueXDir))
  {
    ROS_INFO("IncrementValueX is %f", m_incrementValueXDir);
  }
  else
  {
    ROS_ERROR("Failed to get param 'IncrementValueX'");
    m_incrementValueXDir = 1.0;
  }

  if (m_nh.getParam("/ADDSPlanner/IncrementValueY", m_incrementValueYDir))
  {
    ROS_INFO("IncrementValueY is %f", m_incrementValueYDir);
  }
  else
  {
    ROS_ERROR("Failed to get param 'IncrementValueY'");
    m_incrementValueYDir = 1.0;
  }

  if (m_nh.getParam("/ADDSPlanner/goal_frame_id", m_frameID))
  {
    ROS_INFO("goal_frame_id is %s", m_frameID.c_str());
  }
  else
  {
    ROS_ERROR("Failed to get param 'goal_frame_id'");
    m_frameID = "map";
  }

  m_currentStatus.ocu_input = false;
  m_currentStatus.plan_created = false;
  m_currentStatus.running_survey = false;
  m_currentStatus.survey_complete = false;
  m_currentStatus.errors_occurred = false;
  m_plannerStatusPub.publish(m_currentStatus);

  m_numOfWayPts = 0;

  int index = 0;
  // Used to give goal a timeout (ie if it can't get to goal by this point then give up)
  int numOfCycles = 300;
  int currCycle = 0;
  bool pubNextPoint = true;

  // Flags to be used when for registering results of move_base/results
  m_resultsAreIn = false;
  m_wayPtSucceeded = false;
  m_wayPtMissed = false;
  m_wayPtCancelled = false;

  ros::Rate loop_rate(10);
  while (ros::ok())
  {
    ros::spinOnce();

    if(m_currentStatus.ocu_input && !m_currentStatus.running_survey)
    {
      if(!m_currentStatus.plan_created)
      {
        m_currentStatus.running_survey = true;
        m_plannerStatusPub.publish(m_currentStatus);

        CreatePlan();
        ROS_DEBUG("Number of Waypoints Created:  %d", m_numOfWayPts);
      }
    }
    else if(m_currentStatus.ocu_input && m_currentStatus.running_survey)
    {
      if(m_currentStatus.plan_created)
      {
        // Means all the waypoints have been created in the PoseArray
        // Ready to start publishing them as goals for move_base

        // If cleared to publish a point and the index is less than total
        // number of waypoints created then publish the next goal
        if(pubNextPoint && (index < m_numOfWayPts))
        {
          //cout << endl << "Press Enter to Publish a Waypoint." << endl;
          //cin.ignore();

          ROS_DEBUG("Publishing Waypoint from index #%d of PoseArray", index);
          PublishWaypointGoal(index);
          index++;

          pubNextPoint = false;
          currCycle = 0;
        }

        // If mission is over/completed
        if(pubNextPoint && (index == m_numOfWayPts))
        {
          m_currentStatus.survey_complete = true;
          m_plannerStatusPub.publish(m_currentStatus);
          ROS_INFO("Lawn Mower Sweep of Area Completed.");
          // Ensures message only prints once on console
          index++;

          // Items necessary to prepare for the next mission.
          m_currentStatus.ocu_input = false;
          m_currentStatus.plan_created = false;
          m_currentStatus.running_survey = false;
          m_currentStatus.survey_complete = false;
          m_currentStatus.errors_occurred = false;
          m_plannerStatusPub.publish(m_currentStatus);

          m_numOfWayPts = 0;
          index = 0;

          geometry_msgs::PoseArray emptyArray;
          m_genWayPtsList = emptyArray;

          actionlib_msgs::GoalID clearGoals;
          m_waypointCancelPub.publish(clearGoals);
        }

        // If the waypoint has not been reached by this time, then we are saying
        // it has timed out and we need to move on to the next waypoint goal
        if(currCycle >= numOfCycles)
        {
          ROS_DEBUG("Timeout:  Cancelling waypoint and moving to next");
          std::stringstream idStream;
          idStream << "/waypointGoal_index" << (index - 1);
          string id = idStream.str();

          actionlib_msgs::GoalID goalToCancel;
          goalToCancel.stamp = ros::Time::now();
          goalToCancel.id = id;

          m_waypointCancelPub.publish(goalToCancel);
          currCycle = 0;
        }

        // Check for results of the goal
        if(m_resultsAreIn)
        {
          // If the goal has been met and achieved, then don't wait for time
          // delay.  Go on and publish the next waypoint
          if(m_wayPtSucceeded)
          {
            pubNextPoint = true;
            m_wayPtSucceeded = false;
          }
          // If the goal cannot be reached or errors out, then cancel this goal
          // and move on to the next goal
          if(m_wayPtMissed)
          {
            std::stringstream idStream;
            idStream << "/waypointGoal_index" << (index - 1);
            string id = idStream.str();

            actionlib_msgs::GoalID goalToCancel;
            goalToCancel.stamp = ros::Time::now();
            goalToCancel.id = id;

            //m_waypointCancelPub.publish(goalToCancel);
            pubNextPoint = true;
            m_wayPtMissed = false;
          }
          // If waypoint goal was successfully cancelled, then move on to next goal
          if(m_wayPtCancelled)
          {
            pubNextPoint = true;
            m_wayPtCancelled = false;
          }

          m_resultsAreIn = false;
        }
        // Increment the currentCycle value for timeout
        currCycle++;
      }
    }
    loop_rate.sleep();
  }
}

/**
* @brief Destructor of ADDSPlanner class.
*/
ADDSPlanner::~ADDSPlanner()
{

}

/**
* @brief Callback method for operator input.
*
* OCUBoundsCallback takes in the ROS messge of type OCUInputMsg and
* stores the information in the ADDSPlanner class.  The method assumes
* the operational area boundary corners are sent in the order of bottom
* left, top left, top right, and bottom right.  The launching position
* from the OCUInputMsg is published as the robot's initial pose.  A flag
* is set to represent the OCU input has been recieved and the program can
* create a plan.
*
* @param msg The information from the operator in an adds_planner::OCUInputMsg
*/
void ADDSPlanner::OCUBoundsCallback(adds_planner::OCUInputMsg msg)
{
  // Take the four bounds from OCU and place into Boundary
  // Remember we are assuming the following order:
  // bottom left, top left, top right, bottom right
  ocuBounds.opArea[0].x = msg.bottomLeftX;
  ocuBounds.opArea[0].y = msg.bottomLeftY;

  ocuBounds.opArea[1].x = msg.topLeftX;
  ocuBounds.opArea[1].y = msg.topLeftY;

  ocuBounds.opArea[2].x = msg.topRightX;
  ocuBounds.opArea[2].y = msg.topRightY;

  ocuBounds.opArea[3].x = msg.bottomRightX;
  ocuBounds.opArea[3].y = msg.bottomRightY;

  // Using those four points set the min's and max's for the X,Y values
  m_xMax = ocuBounds.opArea[3].x;
  m_xMin = ocuBounds.opArea[0].x;
  m_yMax = ocuBounds.opArea[1].y;
  m_yMin = ocuBounds.opArea[0].y;

  ROS_DEBUG("X Min:  %f", m_xMin);
  ROS_DEBUG("X Max:  %f", m_xMax);
  ROS_DEBUG("Y Min:  %f", m_yMin);
  ROS_DEBUG("Y Max:  %f", m_yMax);

  //Use this as trigger to create and calculate waypoints
  //ROS_DEBUG("Triggering boolean flag to calculate waypoints.");
  m_currentStatus.ocu_input = true;
  m_plannerStatusPub.publish(m_currentStatus);

  //TODO add handling for origin and launch position

  //Use launch position as initial pose for robot
  m_launchPt.header.stamp = ros::Time::now();
  m_launchPt.header.frame_id = "/initalpose_from_OCU";

  m_launchPt.pose.pose.position.x = msg.launchLocationX;
  m_launchPt.pose.pose.position.y = msg.launchLocationY;
  m_launchPt.pose.pose.position.z = 0;

  m_launchPt.pose.pose.orientation.x = 0;
  m_launchPt.pose.pose.orientation.y = 0;
  m_launchPt.pose.pose.orientation.z = 0;
  m_launchPt.pose.pose.orientation.w = 1;

  m_launchPt.pose.covariance = {0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853891945200942};

  m_launchPtPub.publish(m_launchPt);
}

/**
* @brief Callback method for the results of the waypoint / goal.
*
* WayPtResultCallback takes in a ROS message of type MoveBaseActionResult and
* triggers flags for processing a waypoint/goal that was successfully reached,
* missed, or cancelled.
*
* @param msg The result of the robot's attempt to reach the waypoint/goal
* stored in a move_base_msgs::MoveBaseActionResult
*/
void ADDSPlanner::WayPtResultCallback(move_base_msgs::MoveBaseActionResult msg)
{
  m_resultsAreIn = true;
  // Vehicle has reached the waypoint - Success
  if(msg.status.status == actionlib_msgs::GoalStatus::SUCCEEDED)
  {
    ROS_DEBUG("Waypoint goal has status of SUCCEEDED");
    m_wayPtSucceeded = true;
  }
  // Vehicle has aborted waypoint goal
  else if(msg.status.status == actionlib_msgs::GoalStatus::ABORTED)
  {
    ROS_DEBUG("Waypoint goal has status of ABORTED");
    m_wayPtMissed = true;
  }
  // Vehicle has rejected waypoint goal
  else if(msg.status.status == actionlib_msgs::GoalStatus::REJECTED)
  {
    ROS_DEBUG("Waypoint goal has status of REJECTED");
    m_wayPtMissed = true;
  }
  // Vehicle has lost waypoint goal
  else if(msg.status.status == actionlib_msgs::GoalStatus::LOST)
  {
    ROS_DEBUG("Waypoint goal has status of LOST");
    m_wayPtMissed = true;
  }
  // Waypoint goal is preempted
  else if(msg.status.status == actionlib_msgs::GoalStatus::PREEMPTED)
  {
    ROS_DEBUG("Waypoint goal has status of PREEMPTED");
    m_wayPtCancelled = true;
  }
  // Waypoint goal is recalled
  else if(msg.status.status == actionlib_msgs::GoalStatus::RECALLED)
  {
    ROS_DEBUG("Waypoint goal has status of RECALLED");
    m_wayPtCancelled = true;
  }
  else
  {
    ROS_DEBUG("Waypoint goal has a status other than a terminal state");
  }
}

/**
* @brief Helper method for creating survey plan.
*
* CreatePlan uses CalculateIterations for determining the number of iterations
* for covering the survey area in both the X and Y directions.  The method then
* calculates the x and y values for each waypoint in the lawnmower search
* pattern and sends each coordinate to the method CreateWaypoints.
*/
void ADDSPlanner::CreatePlan()
{
  bool remainderX;
  bool remainderY;
  CalculateIterations(remainderX, remainderY);

  if(remainderX)
    m_numXIterations+=1;
  if(remainderY)
    m_numYIterations+=1;

  ROS_DEBUG("Number of X Iterations:  %i", m_numXIterations);
  ROS_DEBUG("Number of Y Iterations:  %i", m_numYIterations);

  double current_x = m_xMin;
  double current_y = m_yMin;

  bool headingUp = true;

  for(int i = 0; i <= m_numYIterations; i++)
  {
    if(i < m_numYIterations)
      current_y = m_yMin + (i * m_incrementValueYDir);
    else
      current_y = m_yMax;

    for(int j = 0; j <= m_numXIterations; j++)
    {
      if(headingUp)
      {
        if(j < m_numXIterations)
          current_x = m_xMin + (j * m_incrementValueXDir);
        else
          current_x = m_xMax;
      }
      else
      {
        if(j < m_numXIterations)
          current_x = m_xMax - (j * m_incrementValueXDir);
        else
          current_x = m_xMin;
      }

      //Create new waypoint and add to PoseArray
      CreateWaypoints(current_x, current_y);
    }

    headingUp = !headingUp;
  }

  // Set boolean saying that a plan has been created
  m_currentStatus.plan_created = true;
  m_plannerStatusPub.publish(m_currentStatus);
}

/**
* @brief Helper method for calculating the number of iterations.
*
* CalculateIterations uses the operational bounds to calculate the number
* of iterations for covering the survey area in both the X and Y direction.
*
* @param remainderX True if an extra iteration needs to be added in the X
* direction to cover the remainder of the operational area
* @param remainderY True if an extra iteration needs to be added in the Y
* direction to cover the remainder of the operational area
*/
void ADDSPlanner::CalculateIterations(bool &remainderX, bool &remainderY)
{
  double opAreaHeight = m_yMax - m_yMin;
  double opAreaWidth = m_xMax - m_xMin;

  m_numYIterations = opAreaHeight / m_incrementValueYDir;
  m_numXIterations = opAreaWidth / m_incrementValueXDir;

  remainderX = (fmod(opAreaWidth, m_incrementValueXDir) != 0);
  remainderY = (fmod(opAreaHeight, m_incrementValueYDir) != 0);
}

/**
* @brief Helper method for creating waypoints with the Pose structure.
*
* CreateWaypoints takes in the x, y coordinates calculated and creates a Pose
* variable with the x, y position.  The waypoint is stored in the class's
* PoseArray.  The waypoints are grabbed from the PoseArray when the class is
* ready to publix a particular Pose as the goal for the robot.
*
* @param xIn The x coordinate of the position for the new waypoint
* @param yin The y coordinate of the position for the new waypoint
*/
void ADDSPlanner::CreateWaypoints(double xIn, double yIn)
{
  //Set the header information for PoseArray
  m_genWayPtsList.header.stamp = ros::Time::now();
  m_genWayPtsList.header.frame_id = "/lawnmower_search_waypoints";

  geometry_msgs::Pose toBeAddedWayPt;
  //Set the position (geometry_msgs/Point)
  toBeAddedWayPt.position.x = xIn;
  toBeAddedWayPt.position.y = yIn;
  toBeAddedWayPt.position.z = 0;

  //Set orientation (default value, will update in publish method)
  toBeAddedWayPt.orientation.x = 0;
  toBeAddedWayPt.orientation.y = 0;
  toBeAddedWayPt.orientation.z = 0;
  toBeAddedWayPt.orientation.w = 1;

  ROS_DEBUG("Waypoint Created - position: (%f, %f, %f) orientation:  (%f, %f, %f, %f)",
    toBeAddedWayPt.position.x, toBeAddedWayPt.position.y, toBeAddedWayPt.position.z,
    toBeAddedWayPt.orientation.x, toBeAddedWayPt.orientation.y, toBeAddedWayPt.orientation.z,
    toBeAddedWayPt.orientation.w);

  //Added for internal list of waypoint poses
  m_genWayPtsList.poses.push_back(toBeAddedWayPt);
  m_numOfWayPts++;

}

/**
* @brief Publishes the next waypoint as the goal for the robot.
*
* PublishWaypointGoal first updates the orientation of the waypoint pose
* from the default of 0 degrees to the correct orientation based on the
* next waypoint to be published.  The method creates a MoveBaseActionGoal
* message and populates it with the information of the waypoint to be
* published based on the index passed to the method.  Once all the fields
* of MoveBaseActionGoal have been updated, the message is published for the
* robot.
*
* @param indexOfGoal The index of the waypoint to be published as the goal
* pose
*/
void ADDSPlanner::PublishWaypointGoal(int indexOfGoal)
{
    // Update pose now that all waypoints have been calculated
    // m_numOfWayPts-1 because we don't need to update the last waypoint orientation
    if(indexOfGoal < (m_numOfWayPts - 1))
      UpdateOrientation(indexOfGoal);

    // Initialize the goal structure
    move_base_msgs::MoveBaseActionGoal goalToPub;

    std::stringstream idStream;
    idStream << "/waypointGoal_index" << indexOfGoal;
    string id = idStream.str();

    // Fill in header
    goalToPub.header.stamp = ros::Time::now();
    goalToPub.header.frame_id = m_frameID;

    // Fill in goal_id
    goalToPub.goal_id.stamp = goalToPub.header.stamp;
    goalToPub.goal_id.id = id;

    // Fill in goal
    goalToPub.goal.target_pose.header.stamp = goalToPub.header.stamp;
    goalToPub.goal.target_pose.header.frame_id = m_frameID;
    goalToPub.goal.target_pose.pose = m_genWayPtsList.poses[indexOfGoal];

    // Publish Waypoint Goal
    m_waypointPub.publish(goalToPub);

    ROS_INFO("Waypoint Published - position: (%f, %f, %f) orientation:  (%f, %f, %f, %f)",
      goalToPub.goal.target_pose.pose.position.x,
      goalToPub.goal.target_pose.pose.position.y,
      goalToPub.goal.target_pose.pose.position.z,
      goalToPub.goal.target_pose.pose.orientation.x,
      goalToPub.goal.target_pose.pose.orientation.y,
      goalToPub.goal.target_pose.pose.orientation.z,
      goalToPub.goal.target_pose.pose.orientation.w);
}

/**
* @brief Helper method for updating the orientation of a waypoint Pose.
*
* UpdateOrientation updates the orienation of a waypoint Pose in the class's
* PoseArray.  Once the final orientation is determined based on the next
* waypoint in the PoseArray, the ending rotation is calculated in radians.
* The yaw rotation is used to create a Quaternion and that Quaternion gets
* stored as the updated orientation in the waypoint Pose.
*
* @param currentIndex The current index of the Pose to have its orientation
* updated
*/
void ADDSPlanner::UpdateOrientation(int currentIndex)
{
  int futureIndex = currentIndex + 1;

  // x is forward, y is lateral
  double deltaX = m_genWayPtsList.poses[futureIndex].position.x - m_genWayPtsList.poses[currentIndex].position.x;
  double deltaY = m_genWayPtsList.poses[futureIndex].position.y - m_genWayPtsList.poses[currentIndex].position.y;
  double yawInRads;

  // If change in X is 0
  if(deltaX < 0.001 && deltaX > -0.001)
  {
    if(deltaY > 0) //rotate counterclockwise 90deg
      yawInRads = 1.5707;
    else //rotate clockwise 90deg
      yawInRads = -1.5707;
  }
  else //assuming change in Y is 0
  {
      if(deltaX > 0) //no rotation necessary
        yawInRads = 0;
      else // rotate 180deg
        yawInRads = 3.14159;
  }

  // Set Quaternion using roll, pitch, and yaw (units in radians)
  tf2::Quaternion tempQuat;
  tempQuat.setRPY(0, 0, yawInRads);

  // Set orientation off the temporary Quaternion
  m_genWayPtsList.poses[currentIndex].orientation.x = tempQuat.x();
  m_genWayPtsList.poses[currentIndex].orientation.y = tempQuat.y();
  m_genWayPtsList.poses[currentIndex].orientation.z = tempQuat.z();
  m_genWayPtsList.poses[currentIndex].orientation.w = tempQuat.w();

/*ROS_DEBUG("UpdateOrientation:  waypt[%d]:  x: %f, y: %f, z: %f, w: %f", currentIndex,
    m_genWayPtsList.poses[currentIndex].orientation.x,
    m_genWayPtsList.poses[currentIndex].orientation.y,
    m_genWayPtsList.poses[currentIndex].orientation.z,
    m_genWayPtsList.poses[currentIndex].orientation.w);*/
}

// Set a callback for listing to bad material found
//then it needs to move backward to previous location
// pause and wait to continue path again.
// will need to grab the current path planned in /waypoints
