/***************************************************************************//**
 * @author Allison Price
 * @Date 07/24/17
 *
 * @brief Application for generating the adds planner
 *
 * This software was developed at Naval Surface Warfare Center Panama City
 ******************************************************************************/
#include "adds_planner/ADDSPlanner.h"

int main(int argc, char * argv[])
{
  ros::init(argc, argv, "adds_planner");

  ADDSPlanner addsPlanner;

  ros::spin();

  return(0);
}
