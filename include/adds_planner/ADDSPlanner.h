/***************************************************************************//**
 * @author Allison Price
 * @Date 05/17/2019
 * @version 1.0
 *
 * @brief Planner for Autonomous Dry Docks Survey (ADDS)
 *
 * When provided a four point boundary from the operator control unit, the
 * application generates a simple lawnmower waypoint path to be followed.
 *
 * This software was developed at Naval Surface Warfare Center Panama City
 ******************************************************************************/

#ifndef ADDSPLANNER_HEADER
#define ADDSPLANNER_HEADER

#include "ros/ros.h"
#include "adds_planner/OCUInputMsg.h"
#include "adds_planner/PlannerStatus.h"

#include "actionlib_msgs/GoalID.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "move_base_msgs/MoveBaseActionGoal.h"
#include "move_base_msgs/MoveBaseActionResult.h"

class ADDSPlanner
{
  /// Struct used to represent a point with x and y values
  struct Point {
    double x;
    double y;
  };

  /// @brief Struct used to present a boundary that has four points.
  /// It is assumed to be rectangular.
  struct Boundary {
    Point opArea[4];
  };

  public:
    ADDSPlanner();
    ~ADDSPlanner();

    void OCUBoundsCallback(adds_planner::OCUInputMsg msg);
    void WayPtResultCallback(move_base_msgs::MoveBaseActionResult msg);
    void CreatePlan();

  private:
    void CalculateIterations(bool &remainderX, bool &remainderY);
    void CreateWaypoints(double xIn, double yIn);
    void PublishWaypointGoal(int indexOfGoal);
    void UpdateOrientation(int currentIndex);

    /// Class's ros node handler
    ros::NodeHandle m_nh;

    // Subscribers for ADDSPlanner
    /// Subscribes to OCU_BOUNDS topic to get the operational area
    /// bounds from the operator.
    ros::Subscriber m_ocuInputSub;
    /// Subscribes to /move_base/result to listen for the success or
    /// failure of robot to reach it's goal/waypoint
    ros::Subscriber m_wayptResultSub;

    // Publishers for ADDSPlanner
    /// Publishes the launch point from the OCU as the initialpose
    ros::Publisher m_launchPtPub;
    /// Published the waypoints (one at a time) as a goal for the
    /// move_base topic to achieve
    ros::Publisher m_waypointPub;
    /// Publishes the cancellation of a waypoint based on its goal ID
    ros::Publisher m_waypointCancelPub;
    /// Publishes the execution status of the planner
    ros::Publisher m_plannerStatusPub;

    /// Represents the boundary of the operational area to be swept
    Boundary ocuBounds;

    /// Boolean flag to say the results of a waypoint/goal are in
    bool m_resultsAreIn;
    /// Boolean flag representing if the robot is currently running
    /// a survey sweep
    bool m_runningSurvey;

    /// Boolean flag representing a waypoint successfully being reached
    bool m_wayPtSucceeded;
    /// Boolean flag representing a waypoint being missed
    bool m_wayPtMissed;
    /// Boolean flag representing a waypoint being cancelled
    bool m_wayPtCancelled;

    /// The value at which the x component of the waypoint is changed
    double m_incrementValueXDir;
    /// The value at which the y component of the waypoint is changed
    double m_incrementValueYDir;

    /// String for the frame ID of the goal
    std::string m_frameID;

    /// Maximum value of the y component
    double m_yMax;
    /// Minimum value of the y component
    double m_yMin;
    /// Maximum value of the x component
    double m_xMax;
    /// Minimum value of the x component
    double m_xMin;

    /// Number of iterations to be looped through for waypoint generation (y component)
    int m_numYIterations;
    /// Number of iterations to be looped through for waypoint generation (x component)
    int m_numXIterations;

    /// When a waypoint x-y is calculated it gets placed in this array
    geometry_msgs::PoseArray m_genWayPtsList;
    /// Used to set and publish the initial pose
    geometry_msgs::PoseWithCovarianceStamped m_launchPt;

    /// ROS message used to keep track of the ADDS Planner's status
    adds_planner::PlannerStatus m_currentStatus;
    /// Boolean flag representing input has been recieved from the operator
    //bool m_ocuInputReceived;
    /// Boolean flag representing an array of waypoints has been generated
    //bool m_planCreated;
    /// Number of waypoints generated and stored in the PoseArray
    int m_numOfWayPts;
};

#endif
